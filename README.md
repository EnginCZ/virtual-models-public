# Virtual neural networks: hundreds of souls in a body

This repository presents relevant source codes to the paper `Virtual neural networks: hundreds of souls in a body.` This paper realizes a novel paradigm called virtual neural networks where an idea of an ensemble approach of many 'virtual' models that share weights given by a few 'physical' models. The ensemble consists of up to hundreds of virtual models that are trained concurrently. Moreover, all virtual networks share the same input, and their tangled structure creates a kind of inner augmentation that elevates the robustness of the whole ensemble. A more detailed explanation is available in the paper *Virtual neural networks: hundreds of souls in a body* (full reference to the article will be provided after the publication).

The implementation coverts virtual models implementation for EfficientNet architecture. The implementation is realized in Python 3.8 using IPython notebooks.

## Requirements

The basic requirements needed to run this script are:
* Support for IPython notebooks (`ipython` and `ipywidgets` packages at least)
* `numpy`
* `tensorflow-gpu`
* `optuna`
* `wandb` (optional)

The implementation was tested in `venv` environment, and packages were installed using `pip` command. The exact package listing is available in the file `requirements.txt`. 

## Training and evaluation

For training and evaluation, the python notebook file `EfficientNet Cifar.ipynb` is prepared. 
The file is divided into the three main parts:
* Initialization - where the main required packages are imported and required definitions are declared
* Simple training and evaluation - for simple training demonstration; the training is followed by the evaluation script
* Weights-and-biases based training and evaluation - which was used for training and evaluation during the training process; the outputs of this training are provided in the paper

To set up the model, the following cells are important:
* the second cell of the notebook defines the number of physical networks (`PHYSICAL_NET_COUNT`) and virtual networks (`VIRTUAL_NET_COUNT`). Note that the number of physical networks determines the total model size which must fit into the (GPU) memory.
* the third cell defines the used EfficientNet model. One can simply adjust the parameters of the `EfficientNetSettings` object, or use one of the predefined methods `EfficientNetSettings.create_EfficientNetBx(y)`, where `x` determines the EfficientNet B-id (0-7) and  `y` defines expected number of classes.

## Expected results

The code above was used to generate results published in the paper. The following table shows the performance over the CIFAR-10 dataset using EfficientNet B0 and B1. The statistics are given by a single run. Prediction time is for a single batch of 16 images. Virtual-network-ensemble notation is in the `physical-nets-count`/`virtual-nets-count` format; `aver` means the modules were aggregated using the 'average' operation; `best-single` presents results for the best single virtual network taken among all the virtual networks in the pool.

| Network | Test acc [%] | Params [M] | Train mem [GB] | Infer. mem [GB] | Infer. time [ms]|
|---|---|---|---|---|---|
|EfficientNet B0 | 94.97 | 4.02 | 5.66 | 3.35 | 16 |
|EfficientNet B1 | 95.04 | 6.52 | 9.71 | 5.38 | 22 |
|EfficientNet B2 | 95.05 | 7.71 | 9.76 | 5.68 | 26 |
|EfficientNet B0 2/10 aver | 95.72 | 8.04 | 34.20 | 3.35 | 145 |
|EfficientNet B1 2/10 aver | 96.04 | 13.04 | 34.20 | 5.38 | 215 |
|EfficientNet B0 2/10 best single | 95.03 | 4.02 | - | 3.35 | 16 |
|EfficientNet B1 2/10 best single | 95.48 | 6.52 | - | 5.38 | 22 |


## References
The original EfficientNet implementation is taken from xuannianz's EfficientDet: https://github.com/xuannianz/EfficientDet