from typing import List, Dict
from tensorflow.keras import layers
from tensorflow.keras import backend
from tensorflow.keras import models
import math
import warnings
import string
import random
import os


class EfficientNetHelper:
    CONV_KERNEL_INITIALIZER = {
        'class_name': 'VarianceScaling',
        'config': {
            'scale': 2.0,
            'mode': 'fan_out',
            # EfficientNet actually uses an untruncated normal distribution for
            # initializing conv layers, but keras.initializers.VarianceScaling use
            # a truncated distribution.
            # We decided against a custom initializer for better serializability.
            'distribution': 'normal'
        }
    }

    DENSE_KERNEL_INITIALIZER = {
        'class_name': 'VarianceScaling',
        'config': {
            'scale': 1. / 3.,
            'mode': 'fan_out',
            'distribution': 'uniform'
        }
    }

    @staticmethod
    def obtain_input_shape(input_shape,
                           default_size,
                           min_size,
                           data_format,
                           require_flatten,
                           weights=None):
        """Internal utility to compute/validate a model's input shape.

        # Arguments
            input_shape: Either None (will return the default network input shape),
                or a user-provided shape to be validated.
            default_size: Default input width/height for the model.
            min_size: Minimum input width/height accepted by the model.
            data_format: Image data format to use.
            require_flatten: Whether the model is expected to
                be linked to a classifier via a Flatten layer.

        # Returns
            An integer shape tuple (may include None entries).

        # Raises
            ValueError: In case of invalid argument values.
        """
        if weights != 'imagenet' and input_shape and len(input_shape) == 3:
            if data_format == 'channels_first':
                if input_shape[0] not in {1, 3}:
                    warnings.warn(
                        'This model usually expects 1 or 3 input channels. '
                        'However, it was passed an input_shape with ' +
                        str(input_shape[0]) + ' input channels.')
                default_shape = (input_shape[0], default_size, default_size)
            else:
                if input_shape[-1] not in {1, 3}:
                    warnings.warn(
                        'This model usually expects 1 or 3 input channels. '
                        'However, it was passed an input_shape with ' +
                        str(input_shape[-1]) + ' input channels.')
                default_shape = (default_size, default_size, input_shape[-1])
        else:
            if data_format == 'channels_first':
                default_shape = (3, default_size, default_size)
            else:
                default_shape = (default_size, default_size, 3)
        if weights == 'imagenet' and require_flatten:
            if input_shape is not None:
                if input_shape != default_shape:
                    raise ValueError('When setting `include_top=True` '
                                     'and loading `imagenet` weights, '
                                     '`input_shape` should be ' +
                                     str(default_shape) + '.')
            return default_shape
        if input_shape:
            if data_format == 'channels_first':
                if input_shape is not None:
                    if len(input_shape) != 3:
                        raise ValueError(
                            '`input_shape` must be a tuple of three integers.')
                    if input_shape[0] != 3 and weights == 'imagenet':
                        raise ValueError('The input must have 3 channels; got '
                                         '`input_shape=' + str(input_shape) + '`')
                    if ((input_shape[1] is not None and input_shape[1] < min_size) or
                            (input_shape[2] is not None and input_shape[2] < min_size)):
                        raise ValueError('Input size must be at least ' +
                                         str(min_size) + 'x' + str(min_size) +
                                         '; got `input_shape=' +
                                         str(input_shape) + '`')
            else:
                if input_shape is not None:
                    if len(input_shape) != 3:
                        raise ValueError(
                            '`input_shape` must be a tuple of three integers.')
                    if input_shape[-1] != 3 and weights == 'imagenet':
                        raise ValueError('The input must have 3 channels; got '
                                         '`input_shape=' + str(input_shape) + '`')
                    if ((input_shape[0] is not None and input_shape[0] < min_size) or
                            (input_shape[1] is not None and input_shape[1] < min_size)):
                        raise ValueError('Input size must be at least ' +
                                         str(min_size) + 'x' + str(min_size) +
                                         '; got `input_shape=' +
                                         str(input_shape) + '`')
        else:
            if require_flatten:
                input_shape = default_shape
            else:
                if data_format == 'channels_first':
                    input_shape = (3, None, None)
                else:
                    input_shape = (None, None, 3)
        if require_flatten:
            if None in input_shape:
                raise ValueError('If `include_top` is True, '
                                 'you should specify a static `input_shape`. '
                                 'Got `input_shape=' + str(input_shape) + '`')
        return input_shape

    @staticmethod
    def round_filters(filters, width_coefficient, depth_divisor):
        """Round number of filters based on width multiplier."""

        filters *= width_coefficient
        new_filters = int(filters + depth_divisor / 2) // depth_divisor * depth_divisor
        new_filters = max(depth_divisor, new_filters)
        # Make sure that round down does not go down by more than 10%.
        if new_filters < 0.9 * filters:
            new_filters += depth_divisor
        return int(new_filters)

    @staticmethod
    def round_repeats(repeats, depth_coefficient):
        """Round number of repeats based on depth multiplier."""

        return int(math.ceil(depth_coefficient * repeats))

    @staticmethod
    def get_dropout():
        """Wrapper over custom dropout. Fix problem of ``None`` shape for tf.keras.
        It is not possible to define FixedDropout class as global object,
        because we do not have modules for inheritance at first time.

        Issue:
            https://github.com/tensorflow/tensorflow/issues/30946
        """

        # backend, layers, models, keras_utils = get_submodules_from_kwargs(kwargs)

        class FixedDropout(layers.Dropout):
            def _get_noise_shape(self, inputs):
                if self.noise_shape is None:
                    return self.noise_shape

                symbolic_shape = backend.shape(inputs)
                noise_shape = [symbolic_shape[axis] if shape is None else shape
                               for axis, shape in enumerate(self.noise_shape)]
                return tuple(noise_shape)

        return FixedDropout

    @staticmethod
    def get_swish():
        # backend, layers, models, keras_utils = get_submodules_from_kwargs(kwargs)

        def swish(x):
            """Swish activation function: x * sigmoid(x).
            Reference: [Searching for Activation Functions](https://arxiv.org/abs/1710.05941)
            """

            if backend.backend() == 'tensorflow':
                try:
                    # The native TF implementation has a more
                    # memory-efficient gradient implementation
                    return backend.tf.nn.swish(x)
                except AttributeError:
                    pass

            return x * backend.sigmoid(x)

        return swish


class BlockArgs:
    def __init__(self, kernel_size: int, num_repeat: int, input_filters: int, output_filters: int,
                 expand_ratio: int, id_skip: bool, strides: List, se_ratio: float):
        self.kernel_size = kernel_size
        self.num_repeat = num_repeat
        self.input_filters = input_filters
        self.output_filters = output_filters
        self.expand_ratio = expand_ratio
        self.id_skip = id_skip
        self.strides = strides
        self.se_ratio = se_ratio

    def clone(self, input_filters=None, output_filters=None, num_repeat=None, strides=None):
        ret = BlockArgs(
            self.kernel_size,
            self.num_repeat if num_repeat is None else num_repeat,
            self.input_filters if input_filters is None else input_filters,
            self.output_filters if output_filters is None else output_filters,
            self.expand_ratio, self.id_skip,
            self.strides if strides is None else strides,
            self.se_ratio
        )
        return ret


class EfficientNetSettings:
    '''Settings used to define EfficientNet architecture for generator.'''

    DEFAULT_BLOCKS_ARGS = [
        BlockArgs(kernel_size=3, num_repeat=1, input_filters=32, output_filters=16,
                  expand_ratio=1, id_skip=True, strides=[1, 1], se_ratio=0.25),
        BlockArgs(kernel_size=3, num_repeat=2, input_filters=16, output_filters=24,
                  expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
        BlockArgs(kernel_size=5, num_repeat=2, input_filters=24, output_filters=40,
                  expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
        BlockArgs(kernel_size=3, num_repeat=3, input_filters=40, output_filters=80,
                  expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
        BlockArgs(kernel_size=5, num_repeat=3, input_filters=80, output_filters=112,
                  expand_ratio=6, id_skip=True, strides=[1, 1], se_ratio=0.25),
        BlockArgs(kernel_size=5, num_repeat=4, input_filters=112, output_filters=192,
                  expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
        BlockArgs(kernel_size=3, num_repeat=1, input_filters=192, output_filters=320,
                  expand_ratio=6, id_skip=True, strides=[1, 1], se_ratio=0.25)
    ]

    def __init__(self, width_coefficient, depth_coefficient, default_resolution,
                 dropout_rate=0.2, drop_connect_rate=0.2, depth_divisor=8,
                 blocks_args=DEFAULT_BLOCKS_ARGS, model_name='efficientnet',
                 include_top=True, input_tensor=None, input_shape=None,
                 pooling=None, classes=1000, additional_kwargs=None):
        self.width_coefficient = width_coefficient
        self.depth_coefficient = depth_coefficient
        self.default_resolution = default_resolution
        self.dropout_rate = dropout_rate
        self.drop_connect_rate = drop_connect_rate
        self.depth_divisor = depth_divisor
        self.blocks_args = blocks_args
        self.model_name = model_name
        self.include_top = include_top
        self.input_tensor = input_tensor
        self.input_shape = input_shape
        self.pooling = pooling
        self.classes = classes
        self.kwargs = additional_kwargs

    @staticmethod
    def create_EfficientNetB0(number_of_classes: int,
                              model_name='efficientnet-b0',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.0, 1.0, 224, 0.2,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB0(number_of_classes: int,
                              model_name='efficientnet-b0',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.0, 1.0, 224, 0.2,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB1(number_of_classes: int,
                              model_name='efficientnet-b1',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.0, 1.1, 240, 0.2,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB2(number_of_classes: int,
                              model_name='efficientnet-b2',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.1, 1.2, 260, 0.3,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB3(number_of_classes: int,
                              model_name='efficientnet-b3',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.2, 1.4, 300, 0.3,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB4(number_of_classes: int,
                              model_name='efficientnet-b4',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.4, 1.8, 380, 0.4,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB5(number_of_classes: int,
                              model_name='efficientnet-b5',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.6, 2.2, 456, 0.4,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB6(number_of_classes: int,
                              model_name='efficientnet-b6',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            1.8, 2.6, 528, 0.5,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret

    @staticmethod
    def create_EfficientNetB7(number_of_classes: int,
                              model_name='efficientnet-b7',
                              include_top=True, input_tensor=None, input_shape=None,
                              pooling=None, additional_kwargs=None
                              ) -> 'EfficientNetSettings':
        ret = EfficientNetSettings(
            2.0, 3.1, 600, 0.5,
            model_name=model_name,
            include_top=include_top,
            input_tensor=input_tensor, input_shape=input_shape,
            pooling=pooling, classes=number_of_classes, additional_kwargs=additional_kwargs
        )
        return ret


class EfficientNetFactory:

    def __init__(self, settings: EfficientNetSettings):
        self._input_layer = None
        self._ld = {}
        self._settings = settings
        self._physical_network_count = 0

    def __register_input_layer(self, layer):
        assert layer is not None
        self._input_layer = layer

    def __register_layer(self, key, layer):
        if key not in self._ld:
            assert self._physical_network_count == 0
            self._ld[key] = []
        else:
            assert self._physical_network_count == len(self._ld[key])
        assert layer is not None
        physical_index = len(self._ld[key])
        tmp = f"{key}___{physical_index}"
        layer._name = tmp
        self._ld[key].append(layer)

    def get_input_layer(self):
        return self._input_layer

    def __get_layer(self, key, index):
        assert key in self._ld.keys()
        tmp = self._ld[key]
        assert len(tmp) == self._physical_network_count
        assert index < self._physical_network_count
        assert index >= 0
        ret = tmp[index]
        return ret

    def _assembly_network(self, index_provider):
        sett = self._settings

        if sett.input_tensor is None:
            img_input = self.get_input_layer()
        else:
            raise "TODO 11"
            # this part was not rewritten from the original repo

        # Build stem
        x = img_input
        stem_index = index_provider()
        x = self.__get_layer('stem_conv', stem_index)(x)
        x = self.__get_layer('stem_bn', stem_index)(x)
        x = self.__get_layer('stem_activation', stem_index)(x)

        # Build blocks
        num_blocks_total = sum(EfficientNetHelper.round_repeats(block_args.num_repeat,
                                                                sett.depth_coefficient) for block_args in
                               sett.blocks_args)
        block_num = 0
        for idx, block_args in enumerate(sett.blocks_args):
            assert block_args.num_repeat > 0
            # Update block input and output filters based on depth multiplier.
            block_args = block_args.clone(
                input_filters=EfficientNetHelper.round_filters(block_args.input_filters,
                                                               sett.width_coefficient, sett.depth_divisor),
                output_filters=EfficientNetHelper.round_filters(block_args.output_filters,
                                                                sett.width_coefficient, sett.depth_divisor),
                num_repeat=EfficientNetHelper.round_repeats(block_args.num_repeat, sett.depth_coefficient))

            # The first block needs to take care of stride and filter size increase.
            drop_rate = sett.drop_connect_rate * float(block_num) / num_blocks_total
            mb_index = index_provider()
            x = self._assembly_mv_conv_block(x, mb_index, block_args,
                                             drop_rate=drop_rate,
                                             prefix='block{}a_'.format(idx + 1))
            block_num += 1
            if block_args.num_repeat > 1:
                # pylint: disable=protected-access
                block_args = block_args.clone(
                    input_filters=block_args.output_filters, strides=[1, 1])
                # pylint: enable=protected-access
                for bidx in range(block_args.num_repeat - 1):
                    drop_rate = sett.drop_connect_rate * float(block_num) / num_blocks_total
                    block_prefix = 'block{}{}_'.format(
                        idx + 1,
                        string.ascii_lowercase[bidx + 1]
                    )
                    mb_index = index_provider()
                    x = self._assembly_mv_conv_block(x, mb_index, block_args,
                                                     drop_rate=drop_rate,
                                                     prefix=block_prefix)
                    block_num += 1

        # Build top
        final_index = index_provider()
        x = self.__get_layer('top_conv', final_index)(x)
        x = self.__get_layer('top_bn', final_index)(x)
        x = self.__get_layer('top_activation', final_index)(x)
        if sett.include_top:
            x = self.__get_layer('avg_pool', final_index)(x)
            if sett.dropout_rate and sett.dropout_rate > 0:
                x = self.__get_layer('top_dropout', final_index)(x)
            x = self.__get_layer('probs', final_index)(x)
        else:
            if sett.pooling == 'avg':
                x = self.__get_layer('avg_pool', final_index)(x)
            elif sett.pooling == 'max':
                x = self.__get_layer('max_pool', final_index)(x)

        # Ensure that the model takes into account
        # any potential predecessors of `input_tensor`.
        if sett.input_tensor is not None:
            raise "TODO 13"
            # this part was not rewritten from the original repo
        else:
            inputs = img_input

        # Create model.
        model = models.Model(inputs, x, name=sett.model_name)

        return model

    def _assembly_mv_conv_block(self, inputs, mbi, block_args,
                                drop_rate,
                                prefix):
        has_se = (block_args.se_ratio is not None) and (0 < block_args.se_ratio <= 1)
        bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1

        # workaround over non working dropout with None in noise_shape in tf.keras
        Dropout = EfficientNetHelper.get_dropout()

        # Expansion phase
        filters = block_args.input_filters * block_args.expand_ratio
        if block_args.expand_ratio != 1:
            x = self.__get_layer(prefix + 'expand_conv', mbi)(inputs)
            x = self.__get_layer(prefix + 'expand_bn', mbi)(x)
            x = self.__get_layer(prefix + 'expand_activation', mbi)(x)
        else:
            x = inputs

        # Depthwise Convolution
        x = self.__get_layer(prefix + 'dwconv', mbi)(x)
        x = self.__get_layer(prefix + 'bn', mbi)(x)
        x = self.__get_layer(prefix + 'activation', mbi)(x)

        # Squeeze and Excitation phase
        if has_se:
            se_tensor = self.__get_layer(prefix + 'se_squeeze', mbi)(x)

            se_tensor = self.__get_layer(prefix + 'se_reshape', mbi)(se_tensor)
            se_tensor = self.__get_layer(prefix + 'se_reduce', mbi)(se_tensor)
            se_tensor = self.__get_layer(prefix + 'se_expand', mbi)(se_tensor)

            if backend.backend() == 'theano':
                raise "TODO 15"
                # this part was not rewritten from the original repo
            x = self.__get_layer(prefix + 'se_excite', mbi)([x, se_tensor])  # mozna ([x, se_tensor])

        # Output phase
        x = self.__get_layer(prefix + 'project_conv', mbi)(x)
        x = self.__get_layer(prefix + 'project_bn', mbi)(x)
        if block_args.id_skip and \
                all(s == 1 for s in block_args.strides) \
                and block_args.input_filters == block_args.output_filters:
            if drop_rate and (drop_rate > 0):
                x = self.__get_layer(prefix + 'drop', mbi)(x)
            x = self.__get_layer(prefix + 'add', mbi)([x, inputs])  # mozna ([x, se_tensor])

        return x

    def _build_mv_conv_block(self, block_args, activation, drop_rate, prefix):
        """Mobile Inverted Residual Bottleneck."""

        has_se = (block_args.se_ratio is not None) and (0 < block_args.se_ratio <= 1)
        bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1

        # workaround over non working dropout with None in noise_shape in tf.keras
        Dropout = EfficientNetHelper.get_dropout()

        # Expansion phase
        filters = block_args.input_filters * block_args.expand_ratio
        if block_args.expand_ratio != 1:
            self.__register_layer(prefix + 'expand_conv',
                                  layers.Conv2D(filters, 1,
                                                padding='same',
                                                use_bias=False,
                                                kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                                name=prefix + 'expand_conv'))
            self.__register_layer(prefix + 'expand_bn',
                                  layers.BatchNormalization(axis=bn_axis, name=prefix + 'expand_bn'))
            self.__register_layer(prefix + 'expand_activation',
                                  layers.Activation(activation, name=prefix + 'expand_activation'))
        # else:
        #     x = inputs

        # Depthwise Convolution
        self.__register_layer(prefix + 'dwconv',
                              layers.DepthwiseConv2D(block_args.kernel_size,
                                                     strides=block_args.strides,
                                                     padding='same',
                                                     use_bias=False,
                                                     depthwise_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                                     name=prefix + 'dwconv')
                              )
        self.__register_layer(prefix + 'bn',
                              layers.BatchNormalization(axis=bn_axis, name=prefix + 'bn'))
        self.__register_layer(prefix + 'activation', layers.Activation(activation, name=prefix + 'activation'))

        # Squeeze and Excitation phase
        if has_se:
            num_reduced_filters = max(1, int(
                block_args.input_filters * block_args.se_ratio
            ))
            self.__register_layer(prefix + 'se_squeeze',
                                  layers.GlobalAveragePooling2D(name=prefix + 'se_squeeze'))

            target_shape = (1, 1, filters) if backend.image_data_format() == 'channels_last' else (filters, 1, 1)

            self.__register_layer(prefix + 'se_reshape',
                                  layers.Reshape(target_shape, name=prefix + 'se_reshape'))
            self.__register_layer(prefix + 'se_reduce',
                                  layers.Conv2D(num_reduced_filters, 1,
                                                activation=activation,
                                                padding='same',
                                                use_bias=True,
                                                kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                                name=prefix + 'se_reduce'))
            self.__register_layer(prefix + 'se_expand',
                                  layers.Conv2D(filters, 1,
                                                activation='sigmoid',
                                                padding='same',
                                                use_bias=True,
                                                kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                                name=prefix + 'se_expand'))

            if backend.backend() == 'theano':
                raise "TODO 3"
                # this part was not rewritten from the original repo
            self.__register_layer(prefix + 'se_excite',
                                  layers.Multiply(name=prefix + 'se_excite'))

        # Output phase
        self.__register_layer(prefix + 'project_conv',
                              layers.Conv2D(block_args.output_filters, 1,
                                            padding='same',
                                            use_bias=False,
                                            kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                            name=prefix + 'project_conv'))
        self.__register_layer(prefix + 'project_bn',
                              layers.BatchNormalization(axis=bn_axis, name=prefix + 'project_bn'))

        if block_args.id_skip \
                and all(s == 1 for s in block_args.strides) \
                and block_args.input_filters == block_args.output_filters:
            if drop_rate and (drop_rate > 0):
                self.__register_layer(prefix + 'drop',
                                      Dropout(drop_rate,
                                              noise_shape=(None, 1, 1, 1),
                                              name=prefix + 'drop'))
            self.__register_layer(prefix + 'add',
                                  layers.Add(name=prefix + 'add'))

    def __create_physical_layers(self):
        sett = self._settings

        input_shape = EfficientNetHelper.obtain_input_shape(sett.input_shape,
                                                            default_size=sett.default_resolution,
                                                            min_size=32,
                                                            data_format=backend.image_data_format(),
                                                            require_flatten=sett.include_top)

        if sett.input_tensor is None:
            if self.get_input_layer() is None:
                self.__register_input_layer(layers.Input(shape=input_shape, name="input"))
        else:
            raise "TODO 1"
            # this part was not rewritten from the original repo

        bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1
        activation = EfficientNetHelper.get_swish()

        # Build stem
        self.__register_layer('stem_conv', layers.Conv2D(
            EfficientNetHelper.round_filters(32, sett.width_coefficient, sett.depth_divisor), 3,
            strides=(2, 2),
            padding='same',
            use_bias=False,
            kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
            name='stem_conv'))
        self.__register_layer('stem_bn',
                              layers.BatchNormalization(axis=bn_axis, name='stem_bn'))
        self.__register_layer('stem_activation',
                              layers.Activation(activation, name='stem_activation'))

        # Build blocks
        num_blocks_total = sum(EfficientNetHelper.round_repeats(block_args.num_repeat,
                                                                sett.depth_coefficient) for block_args in
                               sett.blocks_args)
        block_num = 0
        for idx, block_args in enumerate(sett.blocks_args):
            assert block_args.num_repeat > 0
            # Update block input and output filters based on depth multiplier.
            block_args = block_args.clone(
                input_filters=EfficientNetHelper.round_filters(block_args.input_filters,
                                                               sett.width_coefficient, sett.depth_divisor),
                output_filters=EfficientNetHelper.round_filters(block_args.output_filters,
                                                                sett.width_coefficient, sett.depth_divisor),
                num_repeat=EfficientNetHelper.round_repeats(block_args.num_repeat, sett.depth_coefficient))

            # The first block needs to take care of stride and filter size increase.
            drop_rate = sett.drop_connect_rate * float(block_num) / num_blocks_total
            self._build_mv_conv_block(block_args,
                                      activation=activation,
                                      drop_rate=drop_rate,
                                      prefix='block{}a_'.format(idx + 1))
            block_num += 1
            if block_args.num_repeat > 1:
                # pylint: disable=protected-access
                block_args = block_args.clone(
                    input_filters=block_args.output_filters, strides=[1, 1])
                # pylint: enable=protected-access
                for bidx in range(block_args.num_repeat - 1):
                    drop_rate = sett.drop_connect_rate * float(block_num) / num_blocks_total
                    block_prefix = 'block{}{}_'.format(
                        idx + 1,
                        string.ascii_lowercase[bidx + 1]
                    )
                    self._build_mv_conv_block(block_args,
                                              activation=activation,
                                              drop_rate=drop_rate,
                                              prefix=block_prefix)
                    block_num += 1

        # Build top
        self.__register_layer('top_conv',
                              layers.Conv2D(
                                  EfficientNetHelper.round_filters(1280, sett.width_coefficient, sett.depth_divisor), 1,
                                  padding='same',
                                  use_bias=False,
                                  kernel_initializer=EfficientNetHelper.CONV_KERNEL_INITIALIZER,
                                  name='top_conv'))
        self.__register_layer('top_bn',
                              layers.BatchNormalization(axis=bn_axis, name='top_bn'))
        self.__register_layer('top_activation',
                              layers.Activation(activation, name='top_activation'))

        if sett.include_top:
            self.__register_layer('avg_pool',
                                  layers.GlobalAveragePooling2D(name='avg_pool'))
            if sett.dropout_rate and sett.dropout_rate > 0:
                self.__register_layer('top_dropout',
                                      layers.Dropout(sett.dropout_rate, name='top_dropout'))

            self.__register_layer('probs',
                                  layers.Dense(sett.classes,
                                               activation='softmax',
                                               kernel_initializer=EfficientNetHelper.DENSE_KERNEL_INITIALIZER,
                                               name='probs'))
        else:
            if sett.pooling == 'avg':
                self.__register_layer('avg_pool',
                                      layers.GlobalAveragePooling2D(name='avg_pool'))
            elif sett.pooling == 'max':
                self.__register_layer('max',
                                      layers.GlobalMaxPooling2D(name='max'))

    def create_new_physical_net(self, assembly_network: bool = False):
        self.__create_physical_layers()
        self._physical_network_count = self._physical_network_count + 1
        ret = None if not assembly_network else self._assembly_network(lambda: self._physical_network_count - 1)
        return ret

    def create_new_virtual_net(self):
        ret = self._assembly_network(lambda: math.floor(random.random() * self._physical_network_count))
        return ret
